'use strict';

/**
 * @ngdoc function
 * @name gsapDemoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gsapDemoApp
 */
angular.module('gsapDemoApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
